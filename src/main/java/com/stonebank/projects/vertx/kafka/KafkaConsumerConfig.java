package com.stonebank.projects.vertx.kafka;

import com.stonebank.projects.vertx.commons.Constants;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Properties;

public class KafkaConsumerConfig {

    public static KafkaConsumer configureConsumer(Vertx vertx, JsonObject config){
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, config.getString(Constants.BOOTSTRAP_SERVER_KEY));
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, config.getString(Constants.GROUP_ID_CONFIG_KEY));
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, config.getString(Constants.AUTO_RESET_CONFIG_KEY));
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, config.getString(Constants.ENABLE_AUTO_COMMIT_KEY));

        KafkaConsumer<String, String> consumer = KafkaConsumer.create(vertx, props);
        return consumer;
    }
}
