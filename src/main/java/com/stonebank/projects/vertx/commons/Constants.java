package com.stonebank.projects.vertx.commons;

public class Constants {

    public static final String KAFKA_CONFIG_KEY = "kafka";
    public static final String GROUP_ID_CONFIG_KEY = "group-id";
    public static final String AUTO_RESET_CONFIG_KEY = "auto-offset-reset";
    public static final String ENABLE_AUTO_COMMIT_KEY = "enable-auto-commit";
    public static final String TOPIC_NAME_CONFIG_KEY = "topic-name";

    public static final String BOOTSTRAP_SERVER_KEY = "bootstrap-server";

}
