package com.stonebank.projects.vertx.verticles;

import com.stonebank.projects.vertx.commons.Constants;
import com.stonebank.projects.vertx.kafka.KafkaConsumerConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.kafka.client.consumer.KafkaConsumer;

public class KafkaVerticle extends AbstractVerticle {

    JsonObject config;
    KafkaConsumer<String, String> consumer;

    @Override
    public void init(Vertx vertx, Context ctx){
        super.init(vertx, context);
        config = ctx.config().getJsonObject(Constants.KAFKA_CONFIG_KEY);
        consumer = KafkaConsumerConfig.configureConsumer(vertx, config);
    }

    @Override
    public void start(){
        String topicName = config.getString(Constants.TOPIC_NAME_CONFIG_KEY);
        consumer.subscribe(topicName, ar -> {
            if (ar.succeeded()) {
                System.out.println("subscribed");
            } else {
                System.out.println("Could not subscribe " + ar.cause().getMessage());
            }
        });

        consumer.handler(record -> {
            System.out.println("Processing key=" + record.key() + ",value=" + record.value() +
                    ",partition=" + record.partition() + ",offset=" + record.offset());
        });
    }

    @Override
    public void stop(){
        consumer.unsubscribe(ar -> {
            if (ar.succeeded()) {
                System.out.println("Consumer unsubscribed");
            }
        });
    }
}
